# Geronimo

This repository contains my modified version of [smol](https://github.com/colorchestra/smol/), a theme for Hugo.

# To browse the source code

This project is pushed to three remote repositories.

* [Codeberg](https://codeberg.org/) is "a non-profit, community-led effort that provides [**Git hosting**](https://codeberg.org/ethan) and [other services](https://docs.codeberg.org/getting-started/what-is-codeberg/) for free and open source projects."
* Codeberg also backs the [Forgejo software](https://forgejo.org/), which I [**self-host for personal use**](https://git.eyoo.link/ethan).
* [Framasoft](https://framasoft.org/) is a French nonprofit organization actively engaged in free culture and social justice movements. Framasoft hosts [**a GitLab instance**](https://framagit.org/ethan).

# To submit feedback, suggest specific changes, etc.

* Email (preferred)
* Codeberg

# Licensing

SPDX-License-Identifier: `MIT`

[MIT (Expat) License](https://choosealicense.com/licenses/mit/)

Copyright &copy; 2021 Ethan Yoo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
